import { Item } from './../Models/Item.Model';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-right-menu',
  templateUrl: './right-menu.component.html',
  styleUrls: ['./right-menu.component.css']
})
export class RightMenuComponent implements OnInit {
 @Input() item:Item
   status: string ="hide";
  constructor() {
    
   }

  ngOnInit() {
  }
  toggle()
{
 if(this.status=="hide") {
  this.status="show";
 }
 else{
  this.status="hide";
 }
}
}
