import { ServerService } from './Server.service';
import { Component,OnInit } from '@angular/core';
import { Item, Subject } from './Models/Item.Model';
import { Response } from '@angular/http';
import { Alert } from 'selenium-webdriver';
import { forEach } from '@angular/router/src/utils/collection';
import { LoadingBarService } from '@ngx-loading-bar/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'OpenSourceAngularProject';
 public rightmenuLoading=false;
  items:Item[];
  constructor(private serverService:ServerService,private loadingBar: LoadingBarService){
  }
  ngOnInit() {
    this.loadingBar.start();
   this.getRightMenu();
  }
  OnSave(){
    this.serverService.StoreServer(this.items).subscribe(
      (response) => {console.log(response)},
      (error) => alert(error)
    );
    ;
  }
  getRightMenu(){
    
    this.serverService.GetCategories().subscribe(
    
      (response) => {this.loadingBar.complete() ;this.items=response;},
      (error) => this.rightmenuLoading=false
    )
  }
 
}
