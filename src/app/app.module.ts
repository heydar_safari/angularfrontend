import { ServerService } from './Server.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { RightMenuComponent } from './right-menu/right-menu.component';
import {Route, RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoadingBarModule } from '@ngx-loading-bar/core';
const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'content/:id', component: ContentComponent},

];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    RightMenuComponent,
    HomeComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    
    LoadingBarModule.forRoot(),
  ],
  providers: [ServerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
