import { Content } from './Models/Content.Model';
import { Injectable } from "@angular/core";
import { Headers ,Http,Response } from "@angular/http";
import 'rxjs/Rx'
import { Item } from "./Models/Item.Model";

@Injectable()
export class ServerService{
    constructor(private htpp:Http){    }
    StoreServer(servers:any[]){
        const header=new Headers({'Content-Type':'application/json'});
        return this.htpp.post('https://first-3b8f1.firebaseio.com/data.json',servers,{headers:header})
    };
    GetCategories(){
          return  this.htpp.get('http://142.93.34.84:3333/api/getcategories').map(
                (response: Response) => {
                  const items: Item[] = response.json();
               
                  return items;
                }
              )
              
 
    }
    GetPost(id:string){
        return this.htpp.get('http://142.93.34.84:3333/api/getPost/'+id).map(
            (resopnse: Response)=>{
            const content:Content=  resopnse.json() 
            
            return content;
    })
}
}