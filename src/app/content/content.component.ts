import { ServerService } from './../Server.service';
import { Content } from './../Models/Content.Model';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import { LoadingBarService } from '@ngx-loading-bar/core';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
content = new Content("","","شکیبایی کنید...","","");
id:string;
paramsubscription: Subscription;
constructor(private route: ActivatedRoute,private serverService:ServerService) { }

ngOnInit() {
  
    this.id= this.route.snapshot.params['id'];
    this.FillContent();
    this.paramsubscription = this.route.params.subscribe(
    (params: Params) => {
      this.id= params['id'] ;
     this.FillContent();
    }

  )
}
FillContent()
{
  this.content= new Content("","","شکیبایی کنید...","","");
  this.serverService.GetPost(this.id).subscribe(
    (response) => {;this.content=response},
    (error) => alert(error)
  )
}
ngOnDestroy() {
this.paramsubscription.unsubscribe();
}

}
