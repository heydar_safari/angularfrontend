export class Item {
    constructor(public id:number,public name:string,public subjects:Subject[]){
    }
}
export class Subject{
    constructor(public id:string,public title:string){
    }
}